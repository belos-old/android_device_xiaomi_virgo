Copyright 2015 - Belos Project

Xiaomi Mi Note
==============

The Xiaomi Mi Note (codenamed _"Virgo"_) is a high-end smartphone from Xiaomi.

Mi Note was announced in January 2015.

Basic   | Mi Note
:-------|:---------------------------------
CPU     | 2.5GHz Quad-Core MSM8974AC
GPU     | Adreno 330
Memory  | 3GB RAM
Shipped Android Version | 4.4.4
Storage | 16/64GB
Battery | 3000 mAh
Display | 5.7" 1920 x 1080 px
Camera  | 13MPx, dual-tone LED Flash

![Xiaomi Mi Note](http://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-note-1.jpg "Xiaomi Mi Note")
